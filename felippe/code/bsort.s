.section .text
.globl bsort
bsort:
        pushl %ebp
        movl %esp, %ebp
        pushl %ebx
        pushl %esi
        pushl %edi
        movl 12(%ebp), %eax     # eax = n
        movl 8(%ebp), %ebx      # ebx = &A[0]
dowhile:
        xorl %ecx, %ecx         # ecx = swapped = 0
        subl $1, %eax
        xorl %esi, %esi         # esi = i = 0
for:
        cmpl %eax, %esi
        jge endfor
        movl (%ebx,%esi,4), %edi # edi = A[i]
        movl 4(%ebx,%esi,4), %edx # edx = A[i+1]
        cmpl %edi, %edx
        jge endif
        movl %edx, (%ebx,%esi,4)
        movl %edi, 4(%ebx,%esi,4)
        movl $1, %ecx
endif:
        addl $1, %esi
        jmp for
endfor:
        testl %ecx, %ecx
        jg dowhile

        popl %edi
        popl %esi
        popl %ebx
        movl %ebp, %esp
        popl %ebp
        ret

.globl main
main:
        pushl %ebp
        movl %esp, %ebp

        subl $44, %esp
        movl $10, 4(%esp)
        leal 8(%esp), %eax
        movl %eax, (%esp)
        movl $1, 8(%esp)
        movl $3, 12(%esp)
        movl $5, 16(%esp)
        movl $4, 20(%esp)
        movl $7, 24(%esp)
        movl $8, 28(%esp)
        movl $2, 32(%esp)
        movl $9, 36(%esp)
        movl $10, 40(%esp)
        movl $2, 44(%esp)
        call bsort
        addl $44, %esp
        ret
