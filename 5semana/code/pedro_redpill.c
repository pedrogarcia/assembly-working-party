#include <stdio.h>

/**
 * Detect if is on virtual machine.
 *
 * Based on: http://invisiblethings.org/tools/redpill.c
 *
 * modified from joanna at invisiblethings.org
 * should compile and run on any Intel based OS
 *
 * http://invisiblethings.org
 */
const int isVirtualMachine(void) {
    unsigned char m[ 6 ];
    unsigned char rpill[] = "\x0f\x01\x0d\x00\x00\x00\x00\xc3";

    *((unsigned*) &rpill[3]) = (unsigned) m;
    ((void(*)()) &rpill)();

    if(m[5] > 0xd0){
        return 1;
    }

    return 0;
}

int main(void) {
    if (isVirtualMachine())
        printf("Running in Virtual Machine.\n");
    else
        printf("Running in Real Machine\n.");

    return 0;
}
