##############################################################################
#
# redpill.s
#
# Detect if this program is running in a virtual or real machine.
# Based on research of Joanna Rutkowska.
# See: http://www.invisiblethings.org/papers/redpill.html
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g redpill.s -o redpill
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################


.section .data

head:
    .asciz "\x1b[H\x1b[2JPerform the dot product between two vectors A and B.\
            \nAuthor: Pedro Garcia.\n"

msgis:
    .asciz "Running in Virtual Machine\n"

msgisnot:
    .asciz "Running in Real Machine\n"

.section .bss
    .lcomm tempbuffer, 32
    .lcomm result, 4

.section .text
.globl main

main:
    pushl   %ebp
    movl    %esp, %ebp
    call    _preamble

    call    _isVirtualMachine

    cmpl    $1, %ebx
    je      equal

    movl    $msgis, (%esp)
    jmp     print
equal:
    movl    $msgisnot, (%esp)
print:
    call puts

    leave
    ret

_isVirtualMachine:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    sidt    0xfffffff8(%ebp)
    movl    0xfffffff8(%ebp), %eax
break:
    cmpl    $0xFF000000, %eax
    jae     VMWARE
    cmpl    $0xe8000000, %eax
    jae     VIRTUALPC
    movl    $0, %ebx
    jmp     EXIT
VIRTUALPC:
    movl    $1, %ebx
    jmp     EXIT
VMWARE:
    movl    $1, %ebx
EXIT:
    leave
    ret

_preamble:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $head, (%esp)
    call    puts

    leave
    ret
