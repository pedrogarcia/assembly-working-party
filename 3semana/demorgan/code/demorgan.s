##############################################################################
#
# demorgan.s
#
# This program generate the true-tables of De Morgan theorem.
#
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g demorgan.s -o dem
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
# Obs: "To answer everyone dont offense your dignity." (Space Ghost)
##############################################################################


.section .data

head:
    .asciz "True Tables from De Morgan Theorem.\nAuthor: Pedro Garcia"

first_demorgan:
    .asciz "\n1th Thorem: !(A & B) == !A | !B\n"

second_demorgan:
    .asciz "\n2th Theorem: !(A | B) == !A & !B\n"

first_true_table_head:
    .asciz "| A      B |  !(A.B)  |  !A + !B |\n"

second_true_table_head:
    .asciz "| A      B |  !(A+B)  |  !A . !B |\n"

true_table_fmt:
    .asciz "| %2d    %2d |    %2d    |     %2d   |\n"

false:
    .int 0

true:
    .int -1

.section .text
.globl main

main:
    pushl   %ebp
    movl    %esp, %ebp
    call    _preamble

    # 1th De Morgan Theorem
    call _first

    # 2th De Morgan Theorem
    call _second

    leave
    ret

_first:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $first_demorgan, (%esp)
    call    puts

    movl    $first_true_table_head, (%esp)
    call    printf

    # evalue and save in stack to print
    movl    false, %eax
    movl    false, %ebx
    call _first_make_table

    # evalue and save in stack to print
    movl    false, %eax
    movl    true, %ebx
    call    _first_make_table

    # evalue and save in stack to print
    movl    true, %eax
    movl    false, %ebx
    call    _first_make_table

    # evalue and save in stack to print
    movl    true, %eax
    movl    true, %ebx
    call    _first_make_table

    leave
    ret

_first_make_table:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $64, %esp

    movl    %eax, 4(%esp)
    movl    %ebx, 8(%esp)

    # processes the Morgan Theorem
    movl    %eax, %ecx
    not     %ecx
    movl    %ebx, %edx
    not     %edx

    # !A + !B
    or      %ecx, %edx
    movl    %edx, 16(%esp)

    # !(A.B)
    and     %ebx, %eax
    notl    %eax
    movl    %eax, 12(%esp)

    # shift to get just the signal (0 or 1)
    shrl $31, 4(%esp)
    shrl $31, 8(%esp)
    shrl $31, 12(%esp)
    shrl $31, 16(%esp)

    # print
    movl    $true_table_fmt, (%esp)
    call    printf

    leave
    ret

_second:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $second_demorgan, (%esp)
    call    puts

    movl    $second_true_table_head, (%esp)
    call    printf

    # evalue and save in stack to print
    movl    false, %eax
    movl    false, %ebx
    call _second_make_table

    # evalue and save in stack to print
    movl    false, %eax
    movl    true, %ebx
    call    _second_make_table

    # evalue and save in stack to print
    movl    true, %eax
    movl    false, %ebx
    call    _second_make_table

    # evalue and save in stack to print
    movl    true, %eax
    movl    true, %ebx
    call    _second_make_table

    leave
    ret

_second_make_table:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $64, %esp

    movl    %eax, 4(%esp)
    movl    %ebx, 8(%esp)

    # processes the Morgan Theorem
    movl    %eax, %ecx
    not     %ecx
    movl    %ebx, %edx
    not     %edx

    # !A . !B
    and     %ecx, %edx
    movl    %edx, 16(%esp)

    # !(A+B)
    or      %ebx, %eax
    notl    %eax
    movl    %eax, 12(%esp)

    # shift to get just the signal (0 or 1)
    shrl $31, 4(%esp)
    shrl $31, 8(%esp)
    shrl $31, 12(%esp)
    shrl $31, 16(%esp)

    # print
    movl    $true_table_fmt, (%esp)
    call    printf

    leave
    ret

_preamble:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $head, (%esp)
    call    puts

    leave
    ret
