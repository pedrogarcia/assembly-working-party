#include <stdio.h>

#define first_demorgan "\nTable 1: 1th Thorem: !(A & B) == !A | !B \n\n"
#define second_demorgan "\n2th Theorem: !(A | B) == !A & !B \n"
#define true_table_head "| A    B |  !(A.B)  |  !A + !B |\n"
#define true_table_fmt  "| %d    %d |     %d    |     %d    |\n"

int main(void) {
    int a, b;
    int n_op_ab, n_op_a_b;
    int n_a, n_b;

    /* primeiro teorema de De Morgan */
    a = 0; b = 0;
    n_op_ab = !(a & b);
    n_a = !a;
    n_b = !b;
    n_op_a_b = n_a | n_b;

    /* should tb TRUE */
    printf("%d", n_op_ab == n_op_a_b);

    /* primeiro teorema de De Morgan */
    a = 0; b = 0;
    n_op_ab = !(a | b);
    n_a = !a;
    n_b = !b;
    n_op_a_b = n_a & n_b;

    /* should tb TRUE */
    printf("%d", n_op_ab == n_op_a_b);

    return 0;
}
