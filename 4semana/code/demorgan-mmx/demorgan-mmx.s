##############################################################################
#
# demorgan-mmx.s
#
# This program generate the true-tables of De Morgan theorem using SIMD
# MMX instructions
#
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g demorgan-mmx.s -o dem
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
# Obs: "To answer everyone dont offense your dignity." (Space Ghost)
##############################################################################


.section .data
.align 16

head:
    .asciz "\x1b[H\x1b[2JTrue Tables from De Morgan Theorem (With MMX).\
            \nAuthor: Pedro Garcia."

first_demorgan:
    .asciz "\n1th Thorem: !(A & B) == !A | !B\n"

second_demorgan:
    .asciz "\n2th Theorem: !(A | B) == !A & !B\n"

first_true_table_head:
    .asciz "| A      B |  !(A.B)  |  !A + !B |\n"

second_true_table_head:
    .asciz "| A      B |  !(A+B)  |  !A . !B |\n"

true_table_fmt:
    .asciz "| %2d    %2d |    %2d    |     %2d   |\n"

A:
    .short 0, 0, -1, -1

B:
    .short 0, -1, 0, -1

TRUE:
    .short -1, -1, -1, -1

.section .bss
    .lcomm column1, 8
    .lcomm column2, 8

.section .text
.globl main

main:
    pushl   %ebp
    movl    %esp, %ebp
    call    _preamble

    # 1th De Morgan Theorem
    call _first

    # 2th De Morgan Theorem
    call _second

    leave
    ret

_first:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movq    A, %mm0
    movq    B, %mm1

    # calcula !(A & B)
    movq    %mm0, %mm2        # %mm2 := A
    pand    %mm1, %mm2        # %mm2 := (A & B)
    pxor    TRUE, %mm2        # %mm2 := !(A & B) == (A & B) ^ 1

    # calcula !A | !B
    movq    %mm0, %mm3        # %mm3 := A
    pxor    TRUE, %mm3        # %mm3 := %mm3 ^ 1 == !A
    movq    %mm1, %mm4        # %mm4 := B
    pxor    TRUE, %mm4        # %mm4 := %mm4 ^ 1 == !B
    por     %mm4, %mm3        # %mm3 := %mm3 | %mm4 == !A | !B

    # get only the signal
    psrlw   $15, %mm2
    psrlw   $15, %mm3

    # send results to buffer (used to print results)
    movq    %mm2, column1
    movq    %mm3, column2

    call _print_first        # print the first De Morgan theorem tables

    leave
    ret

_print_first:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $64, %esp

    movl    $first_demorgan, (%esp)
    call    puts

    movl    $first_true_table_head, (%esp)
    call    printf

    movl    $true_table_fmt, (%esp)
    movl    $0, %edi

  loop:
    movl    $0, %eax
    movl    $0, %ebx
    movl    $0, %ecx
    movl    $0, %edx

    movsx   A(, %edi, 2), %eax         # move 16 bits to a 32 bits register
    movsx   B(, %edi, 2), %ebx
    movsx   column1(, %edi, 2), %ecx
    movsx   column2(, %edi, 2), %edx

    shrl    $31, %eax            # select only the significant bit
    shrl    $31, %ebx            # if 1, is true, if 0, false

    movl    %eax, 4(%esp)
    movl    %ebx, 8(%esp)
    movl    %ecx, 12(%esp)
    movl    %edx, 16(%esp)

    call    printf

    inc %edi
    cmpl $4, %edi
    jne loop

    leave
    ret


_second:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movq    A, %mm0
    movq    B, %mm1

    # calcula !(A & B)
    movq    %mm0, %mm2        # %mm2 := A
    por     %mm1, %mm2        # %mm2 := (A & B)
    pxor    TRUE, %mm2        # %mm2 := !(A & B) == (A & B) ^ 1

    # calcula !A | !B
    movq    %mm0, %mm3        # %mm3 := A
    pxor    TRUE, %mm3        # %mm3 := %mm3 ^ 1 == !A
    movq    %mm1, %mm4        # %mm4 := B
    pxor    TRUE, %mm4        # %mm4 := %mm4 ^ 1 == !B
    pand    %mm4, %mm3        # %mm3 := %mm3 | %mm4 == !A | !B

    # get only the signal
    psrlw   $15, %mm2
    psrlw   $15, %mm3

    # send results to buffer (used to print results)
    movq    %mm2, column1
    movq    %mm3, column2

    call _print_second        # print the second De Morgan theorem tables

    leave
    ret

_print_second:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $64, %esp

    movl    $second_demorgan, (%esp)
    call    puts

    movl    $second_true_table_head, (%esp)
    call    printf

    movl    $true_table_fmt, (%esp)
    movl    $0, %edi

  loop2:
    movl    $0, %eax
    movl    $0, %ebx
    movl    $0, %ecx
    movl    $0, %edx

    movsx   A(, %edi, 2), %eax  # move 16 bits to a 32 bits register
    movsx   B(, %edi, 2), %ebx
    movsx   column1(, %edi, 2), %ecx
    movsx   column2(, %edi, 2), %edx

    shrl    $31, %eax            # select only the significant bit
    shrl    $31, %ebx            # if 1, is true, if 0, false

    movl    %eax, 4(%esp)
    movl    %ebx, 8(%esp)
    movl    %ecx, 12(%esp)
    movl    %edx, 16(%esp)

    call    printf

    inc %edi
    cmpl $4, %edi
    jne loop2

    leave
    ret



_preamble:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $head, (%esp)
    call    puts

    leave
    ret
