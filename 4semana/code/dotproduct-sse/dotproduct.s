##############################################################################
#
# dotproduct.s
#
# This program perform the dot product between two vectors A and B without
# loops, using Streaming SIMD Extension (SSE3) instructions
#
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g dotproduct.s -o dotproduct
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
# Obs: "To answer everyone dont offense your dignity." (Space Ghost)
##############################################################################


.section .data

head:
    .asciz "\x1b[H\x1b[2JPerform the dot product between two vectors A and B.\
            \nAuthor: Pedro Garcia.\n"

block:
    .int 4

fmt:
    .asciz "Dot product: A.B = %f\n"

A:
    .asciz "A = [-1.2, -1.3, -1.14, 00.1, 1.2, 2, 4, 7]"

B:
    .asciz "B = [12, 1.4, -1.1, 100, 12, 13, 1, 0]\n"

.align 32
vectorA:
    .float -1.2, -1.3, -1.14, 00.1, 1.2, 2, 4, 7

.align 32
vectorB:
    .float 12, 1.4, -1.1, 100, 12, 13, 1, 0

.section .bss
    .lcomm tempbuffer, 32
    .lcomm result, 4

.section .text
.globl main

main:
    pushl   %ebp
    movl    %esp, %ebp
    call    _preamble

    movl    $0, %edi
    movups  vectorA(,%edi,4), %xmm0
    movups  vectorB(,%edi,4), %xmm1
    mulps  %xmm1, %xmm0

    addl    $4, %edi
    movups  vectorA(,%edi,4), %xmm2
    movups  vectorB(,%edi,4), %xmm3
    mulps  %xmm3, %xmm2

    movl    $0, %edi
    movups  %xmm0, tempbuffer(,%edi,4)
    addl    $4, %edi
    movups  %xmm2, tempbuffer(,%edi,4)

    movl    $0, %edi
    
    finit
    fldz

    loop:
      fadd    tempbuffer(,%edi,4)

      inc %edi
      cmpl $8, %edi
      jne loop

    call _print
    
    leave
    ret

_print:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $64, %esp

    movl    $A, (%esp)
    call puts

    movl    $B, (%esp)
    call puts

    movl    $fmt, (%esp)
    fstl    4(%esp)
    call    printf

    breka:
    nop

    leave
    ret

_preamble:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $8, %esp

    movl    $head, (%esp)
    call    puts

    leave
    ret
