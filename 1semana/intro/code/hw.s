	.file	"hw.c"
	.text
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx
	subl	$16, %esp
	movl	$3, -8(%ebp)
	movl	$8, -12(%ebp)
	movl	-8(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -16(%ebp)
	movl	$0, %eax
	addl	$16, %esp
	popl	%ebx
	popl	%ebp
	ret
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.4.1-4ubuntu8) 4.4.1"
	.section	.note.GNU-stack,"",@progbits
