#include <stdio.h>

int soma(const int i, const int k) {
    return i + k;    
}

int multiplica1(const int i, const int j) {
    int k, mul;

    mul = 0;
    for (k = 0; k < i; k++)
        mul += soma(mul, j);
    
    return mul;
}

int main(void) {
    int i;

    for (i = 0; i < 10000; i++)
        soma(i, i);

   for (i = 0; i < 10000; i++)
        multiplica1(i, i);

    return 0;
}
