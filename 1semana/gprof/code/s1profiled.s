	.file	"sample1.c"
	.text
.globl soma
	.type	soma, @function
soma:
	pushl	%ebp
	movl	%esp, %ebp
	call	mcount
	movl	12(%ebp), %eax
	movl	8(%ebp), %edx
	leal	(%edx,%eax), %eax
	popl	%ebp
	ret
	.size	soma, .-soma
.globl multiplica1
	.type	multiplica1, @function
multiplica1:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$24, %esp
	call	mcount
	movl	$0, -8(%ebp)
	movl	$0, -4(%ebp)
	jmp	.L4
.L5:
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, (%esp)
	call	soma
	addl	%eax, -8(%ebp)
	addl	$1, -4(%ebp)
.L4:
	movl	-4(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	.L5
	movl	-8(%ebp), %eax
	leave
	ret
	.size	multiplica1, .-multiplica1
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$24, %esp
	call	mcount
	movl	$0, -4(%ebp)
	jmp	.L8
.L9:
	movl	-4(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	soma
	addl	$1, -4(%ebp)
.L8:
	cmpl	$9999, -4(%ebp)
	jle	.L9
	movl	$0, -4(%ebp)
	jmp	.L10
.L11:
	movl	-4(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	multiplica1
	addl	$1, -4(%ebp)
.L10:
	cmpl	$9999, -4(%ebp)
	jle	.L11
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.4.1-4ubuntu8) 4.4.1"
	.section	.note.GNU-stack,"",@progbits
