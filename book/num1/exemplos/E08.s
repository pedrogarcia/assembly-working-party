##############################################################################
#
# E08.s
#
# How to use the SBB instruction to sum two large integers.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Mar, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E08.s -o E08
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################
.section .data
var1:
    .quad 4294967296
var2:
    .quad 5000000000

stringout:
    .asciz "\x1b[H\x1b[2JSoma: 4294967296 - 5000000000 = %qd\n"

.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    # prepare a "big number" putting the var1 in 2 registers: eax and ebx
    movl    var2, %ebx          # ebx = var1[1]
    movl    var2+4, %eax        # eax = var1[0]

    # prepare another "big number" with 2 registers: ecx and edx
    movl    var1, %edx          # edx = var2[1]
    movl    var1+4, %ecx        # ecx = var2[0]

    # perform the normal add instruction with less significants bits
    subl    %ebx, %edx          # edx = var2[1] + var1[1]

    # here, the sbb instruction get the carry-bit of add above and "copy"
    # to the most significant bit
    sbbl    %eax, %ecx          # ecx = var2[0] + var1[0]

    # put the result on stack to be printed (here, the printf get the 
    # continuous 64 bits)
    pushl   %ecx
    pushl   %edx

    push    $stringout
    call    printf

    pushl   $0
    call    exit

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
