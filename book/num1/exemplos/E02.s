##############################################################################
#
# E02.s
#
# Second exemple of Chapter of "Integer Arithmetic". 
# This program show how move integer data between registers.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E02.s -o E02
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
# Pedro, are you crazy man?
##############################################################################

.section .data
  data:
    .byte 32

.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movb    data, %bl
    movzx   %bl, %eax   # move data to larger size register

    movl    $1, %eax
    movl    $0, %ebx
    int     $0x80

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
