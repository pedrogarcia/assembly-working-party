##############################################################################
#
# E09.s
#
# How to use the INC and DEC instrucions.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Mar, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E09.s -o E09
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################
.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movl    $0, %eax
    dec     %eax

    movw    $12, %bx
    inc     %bx

    movl    $-1, %ecx
    inc     %ecx

    movb    $127, %dl
    inc     %dl

break:

    pushl $0
    call exit

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
