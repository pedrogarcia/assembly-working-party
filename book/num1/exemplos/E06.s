##############################################################################
#
# E06.s
#
# This program show is a example of use sub instruction.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E06.s -o E06
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################
.section .data
variable:
    .int -40

.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movw    $0, %ax         # ax = 0
    movl    variable, %ebx  # ebx = variable; (variable == 40)
    movl    $0, %ecx        # ecx = 0
    movb    $20, %bl        # bl = 20

    subb    $10, %bl        # bl += 10
    subw    $255, %ax       # ax += 255
    subl    $1024, %ecx     # ecx += 1024
    subl    %ecx, %ebx      # ebx += ecx;

break:                      # used to debug. Break here to look ax, ebx & ecx

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
