##############################################################################
#
# E07.s
#
# This program show is a example of use sub instruction.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Mar, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E07.s -o E07
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################
.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movl    $1, %eax
    movl    $0, %edx
    movl    $1, %ebx

    subl    %eax, %edx          # edx = edx - eax == -eax
    neg     %ebx                # ebx = -ebx

    break:                  # used to debug. Break here to look ax, ebx & ecx

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
