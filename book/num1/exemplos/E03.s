##############################################################################
#
# E03.s
#
# Third exemple of Chapter of "Integer Arithmetic". 
# This program show how extend signed and unsigned integers, and how this
# instructions affects the extended operand value.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E03.s -o E03
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
##############################################################################

.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movl    $0, %eax        # "clean" eax
    movl    $-1, %ebx       # put a "trash" in ebx
    movl    $-1, %ecx       # put a "trash" in ecx

    movw    $-100, %ax      # mov 100 to 16 bits variable
    movzx   %ax, %ebx       # to ebx we expect a erroneous value
    movsx   %ax, %ecx       # ecx should be correct

break:                      # used to debug. Break here to look ax, ebx & ecx

    movl    $1, %eax
    movl    $0, %ebx
    int     $0x80

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
