##############################################################################
#
# E01.s
#
# First exemple of Chapter of "Integer Arithmetic". 
# This program uses non-arithmetics instructions to perform arithmetic 
# operations.
#
# Author: PEDRO GARCIA
# <sawp@sawp.com.br>
# @see: http://www.sawp.com.br
#
# Feb, 2010
#
# Writed to:
#   Universidade de Brasilia
#   Instituto de Fisica
#   LCC - Laboratorio de Calculos Cientificos
#   1th ASM Working Party
#
#
# Compile with: gcc -g E01.s -o E01
# Tested with: gcc (Ubuntu 4.4.1-4ubuntu9) 4.4.1
#              icc (ICC) 11.0 20090131
#
# License: Creative Commons
#      <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
# 
# Pedro, are you crazy man?
##############################################################################

.section .text
.globl main
main:
    nop
    pushl   %ebp
    movl    %esp, %ebp

    movl    $1, %eax
    movl    %eax, %ebx
    not     %ebx
    not     %ebx

break1:
    nop

    addl    %eax, %ebx

break2:
    nop

    movl    $1, %eax
    movl    $0, %ebx
    int     $0x80

    movl    %ebp, %esp
    popl    %ebp
    leave
    ret
